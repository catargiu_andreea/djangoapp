from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class UserProfileInfo(models.Model):

    ONE_MONTH, SIX_MONTH = ('one_month', 'six_month')
    SUBSCRIPTION_CHOICES = (
        (ONE_MONTH, 'One Month'),
        (SIX_MONTH, 'Six Months'),
    )

    user = models.OneToOneField(User,on_delete=models.CASCADE)
    payment_type = models.CharField(choices=SUBSCRIPTION_CHOICES, max_length=100, default=ONE_MONTH, verbose_name='Subscription Type')

    def __str__(self):
        return self.user.username
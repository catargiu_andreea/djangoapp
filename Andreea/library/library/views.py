import datetime

from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import TemplateView
from .utiles import *

from .models import Books, BorrowedBooks

mediator = Mediator()

def index(request):
    if request.user.is_authenticated:
        return render(request, 'welcomePage.html')
    else:
        return redirect('/auth/login/')


class BorrowBooksList(View):
    def get(self, request, *args, **kwargs):
        books = Books.objects.all()

        filter_query = request.GET.get('filter', None)
        type_query = request.GET.get('type', None)
        if not filter_query:
            return render(request, "Library.html", {'books_list': books})
        else:
            if type_query == 'genre':
                try:
                    req = RequestMyBooksGenreHandle
                    return mediator.mediate(req)
                except Exception as e:
                    return render(request, "Library.html", {'error': True})
            elif type_query == 'pub_date':
                try:
                    req = RequestMyBooksPubDateHandle
                    return mediator.mediate(req)
                except Exception as e:
                    return render(request, "Library.html", {'error': True})

    def post(self, request, *args, **kwargs):
        book_id = request.POST.get('book_id', None)
        if not book_id:
            return render(request, 'Library.html', {'error': True})
        else:
            book = Books.objects.get(pk=book_id)
            if book.availability:
                req = AvailableBooksHandle
                return mediator.mediate(req)
            else:
                book.book_queue += str(request.user.id) + ';'
                book.save()
                return render(request, 'Library.html', {'queue_added': True})


class MyBooks(View):
    def get(self, request, *args, **kwargs):
        book_list = BorrowedBooks.objects.filter(user=request.user)
        if not book_list:
            return render(request, "myBooks.html", {'no_borrowed': True})
        book_list = [item.book for item in book_list]
        context = {
            'book_list': book_list
        }
        return render(request, "myBooks.html", context=context)

    def post(self, request, *args, **kwargs):
        book_id = request.POST.get('book_id', None)
        if not book_id:
            return render(request, "myBooks.html", {'error': True})
        book = Books.objects.get(pk=book_id)
        book_borrowed = BorrowedBooks.objects.filter(user=request.user, book=book)
        book_borrowed.delete()
        queue = list(filter(None, book.book_queue.split(';')))
        if len(queue) > 0:
            req = MyBooksHandle
            return mediator.mediate(req)
        elif not len(queue):
            book.book_queue = ''
            book.availability = True
            book.save()
            book_borrowed.delete()
            return render(request, "myBooks.html", {'success': True})
        else:
            return render(request, "myBooks.html", {'error': True})


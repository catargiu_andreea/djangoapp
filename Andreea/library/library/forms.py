from django import forms
from . import models

class AddBorrowedBook(forms.ModelForm):
    class Meta:
        model = models.BorrowedBooks
        fields = ['Borrow']


class MyBooks(forms.ModelForm):
    class Meta:
        model = models.BorrowedBooks

from .models import Books, BorrowedBooks
import datetime

from django.contrib.auth.models import User
from django.shortcuts import render, redirect


class RequestMyBooksGenre:
    def __init__(self, request, filter, ord):
        self.filter = filter
        self.request = request
        self.ord = ord
    def getFilter(self):
        return self.filter
    def getRequest(self):
        return self.request
    def getOrd(self):
        return self.ord


class RequestMyBooksGenreHandle:
    def handle(self, requestObj):

        filter_query = requestObj.GET.get('filter', None)
        type_query = requestObj.GET.get('type', None)
        if (type_query == 'genre'):
            books = Books.objects.filter(genre=str(requestObj.filter))
            return render(requestObj, "Library.html", {
                'books_list': books,
                'is_filtered_applied': True,
                'filter': type_query + ' = ' + filter_query
            })

class RequestMyBooksPubDate:
    def __init__(self, request, filter, ord):
        self.filter = filter
        self.request = request
        self.ord = ord
    def getFilter(self):
        return self.filter
    def getRequest(self):
        return self.request
    def getOrd(self):
        return self.ord

class RequestMyBooksPubDateHandle:
    def handle(self, requestObj):

        filter_query = requestObj.GET.get('filter', None)
        type_query = requestObj.GET.get('type', None)
        if (type_query == 'pub_date'):
            now = datetime.datetime.now()
            earlier = now - datetime.timedelta(days=60)
            # Most recent means that the book was published in the last 60 days
            books = Books.objects.filter(pub_date__range=(earlier, now))
            return render(requestObj, "Library.html", {
                'books_list': books,
                'is_filtered_applied': True,
                'filter': type_query + ' = ' + filter_query
            })


class AvailableBooks:
    def __init__(self, request, book_id, ord):
        self.book_id = book_id
        self.request = request
        self.ord = ord

    def getFilter(self):
        return self.filter

    def getRequest(self):
        return self.request

    def getOrd(self):
        return self.ord

class AvailableBooksHandle:
    def handle(self, requestObj):
        book = Books.objects.get(pk=book_id)
        book.book_queue += str(requestObj.user.id) + ';'
        book.availability = not book.availability
        borrowed_book = BorrowedBooks(book=book, user=requestObj.user)
        borrowed_book.save()
        book.save()
        return render(requestObj, 'Library.html', {'success': True})

class MyBooks:
    def __init__(self, request, book_id, ord):
        self.book_id = book_id
        self.request = request
        self.ord = ord

    def getFilter(self):
        return self.filter

    def getRequest(self):
        return self.request

    def getOrd(self):
        return self.ord


class MyBooksHandle:
    def handle(self, requestObj):
        book = Books.objects.get(pk=book_id)
        book_borrowed = BorrowedBooks.objects.filter(user=requestObj.user, book=book)
        book_borrowed.delete()
        queue = list(filter(None, book.book_queue.split(';')))
        next_user_id = queue[0]
        del queue[0]
        next_user = User.objects.get(pk=int(next_user_id))
        import pdb;
        pdb.set_trace()
        book.book_queue = ''
        for item in queue:
            book.book_queue += str(item) + ';'
        book.availability = False
        book.save()
        book_borrowed.delete()
        new_borrowed = BorrowedBooks(user=next_user, book=book)
        new_borrowed.save()
        return render(requestObj, "myBooks.html", {'success': True})

handleDictio = {RequestMyBooksGenre: RequestMyBooksGenreHandle, RequestMyBooksPubDate: RequestMyBooksPubDateHandle, MyBooks : MyBooksHandle}

class Mediator:
    def mediate(self, reqObj):
    	return handleDictio[reqObj.__class__]().handle(reqObj)
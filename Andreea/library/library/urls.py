from django.conf.urls import url

from library import views

app_name = 'library'


urlpatterns = [
    url(r'^library/', views.BorrowBooksList.as_view(), name='library'),
    url(r'^my-books/', views.MyBooks.as_view(), name='my-books'),
    url(r'^welcome/', views.index, name='home'),
]
from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Books(models.Model):
    title = models.CharField(max_length=150)
    author = models.CharField(max_length=150)
    genre = models.CharField(max_length=150)
    pub_date = models.DateField()
    availability = models.BooleanField(default=True)
    book_queue = models.CharField(max_length=500, default="")

    def __str__(self):
        return self.title


class BorrowedBooks(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    book = models.ForeignKey(Books, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id) + '. User: "' + str(self.user.username) + '" Book : "' + str(self.book) + '"'



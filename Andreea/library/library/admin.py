from django.contrib import admin

# Register your models here.
from library.models import Books, BorrowedBooks

admin.site.register(Books)
admin.site.register(BorrowedBooks)
